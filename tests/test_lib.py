import lib;

def test_bookmark_presence():
    for i in lib.db:
            assert(not lib.check_ref(i[0], 'Nonexistent'))

def test_find_sdm_refs():

    i = """--  Intel SDM Vol. 3A, "sect1" some other text
           This is some text Intel SDM Vol. 1, "sect2"   some other text"""

    res = lib.find_sdm_refs(i);
    assert(len(res) == 2)
    assert(res[0] == ('Intel SDM Vol. 3A', '"sect1"'))
    assert(res[1] == ('Intel SDM Vol. 1', '"sect2"'))

    # Newlines
    i = 'Please see Intel SDM\nVol. 3C,\n"sect22"'

    res = lib.find_sdm_refs(i);
    assert(len(res) == 1)
    assert(res[0] == ('Intel SDM Vol. 3C', '"sect22"'))

    i = """<some_element>
           This is a text with a reference

           to the Intel SDM Vol. 4, "sect8888"
           </some_element>
        """

    res = lib.find_sdm_refs(i);
    assert(len(res) == 1)
    assert(res[0] == ('Intel SDM Vol. 4', '"sect8888"'))

    # Ada comment with line-breaks
    i = """--
           --  Record type, see Intel SDM
           --  Vol. 3A,
           --  "sect88"
        """

    res = lib.find_sdm_refs(i);
    assert(len(res) == 1)
    assert(res[0] == ('Intel SDM Vol. 3A', '"sect88"'))

    # Ada comment with line-breaks, single space
    i = """-- Record type, see Intel SDM
           -- Vol. 3D,
           -- "sect4"
        """

    res = lib.find_sdm_refs(i);
    assert(len(res) == 1)
    assert(res[0] == ('Intel SDM Vol. 3D', '"sect4"'))

    # C, Asm
    i = """// Intel SDM
           //    Vol. 3A,
           //        "sect456"
        """

    res = lib.find_sdm_refs(i);
    assert(len(res) == 1)
    assert(res[0] == ('Intel SDM Vol. 3A', '"sect456"'))

    # Muen doc style comment
    i = """--D
           --D Record type, see Intel SDM
           --D Vol. 4,
           --D "sect99"
        """

    res = lib.find_sdm_refs(i);
    assert(len(res) == 1)
    assert(res[0] == ('Intel SDM Vol. 4', '"sect99"'))

def test_find_vtd_refs():

    i = """--  Intel VT-d Specification, "sect44" some other text
           This is some text Intel VT-d Specification, "sect2"   some other text"""

    res = lib.find_vtd_refs(i);
    assert(len(res) == 2)
    assert(res[0] == ('Intel VT-d Specification', '"sect44"'))
    assert(res[1] == ('Intel VT-d Specification', '"sect2"'))

def test_check_ref():
    assert(not lib.check_ref('Intel SDM Vol. 1', '"23.2 No such section"'))
    assert(lib.check_ref('Intel SDM Vol. 1', '"13.4 XSAVE Area"'))
    assert(lib.check_ref('Intel SDM Vol. 1', '"A.1 EFLAGS and Instructions"'))

    assert(not lib.check_ref('Intel SDM Vol. 1', '"A.1 EFLAGS and Instruc"'))
    assert(not lib.check_ref('Intel SDM Vol. 3A', '"6.10 Interrupt Descriptor Table (IDT'))

    assert(not lib.check_ref('Intel VT-d Specification', '"23.2 No such section"'))
    assert(lib.check_ref('Intel VT-d Specification', '"10.4.33 Page Request Queue Address Register"'))
