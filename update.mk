ifeq (,$(SDMPATH))
$(error SDMPATH variable not specified)
endif

ifeq (,$(OUTPATH))
$(error OUTPATH variable not specified)
endif

SDMS := $(wildcard $(SDMPATH)/*.pdf)
ifeq (,$(SDMS))
$(error No PDFs found in $(SDMPATH))
endif

DB := $(patsubst %.pdf,%.bm,$(SDMS))

all: $(DB)
	mv $(SDMPATH)/*.bm $(OUTPATH)/

%.bm: %.pdf
	pdftk $< dump_data_utf8 output $@
