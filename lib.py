import re
import os

sdmv = '2019-may'
vtdv = '2019-june'
db = [('Intel SDM Vol. 1',  'sdm/' + sdmv + '/253665-sdm-vol-1.bm'),
      ('Intel SDM Vol. 2A', 'sdm/' + sdmv + '/253666-sdm-vol-2a.bm'),
      ('Intel SDM Vol. 2B', 'sdm/' + sdmv + '/253667-sdm-vol-2b.bm'),
      ('Intel SDM Vol. 2C', 'sdm/' + sdmv + '/326018-sdm-vol-2c.bm'),
      ('Intel SDM Vol. 2D', 'sdm/' + sdmv + '/334569-sdm-vol-2d.bm'),
      ('Intel SDM Vol. 3A', 'sdm/' + sdmv + '/253668-sdm-vol-3a.bm'),
      ('Intel SDM Vol. 3B', 'sdm/' + sdmv + '/253669-sdm-vol-3b.bm'),
      ('Intel SDM Vol. 3C', 'sdm/' + sdmv + '/326019-sdm-vol-3c.bm'),
      ('Intel SDM Vol. 3D', 'sdm/' + sdmv + '/332831-sdm-vol-3d.bm'),
      ('Intel SDM Vol. 4',  'sdm/' + sdmv + '/335592-sdm-vol-4.bm'),
      ('Intel VT-d Specification',  'vtd/' + vtdv + '/vt-directed-io-spec.bm')]


def get_bookmarks(vol):
    for e in db:
        if e[0] == vol:
            return e[1]

    raise ValueError('No bookmarks for vol. ' + vol)


def read_file(path):
    """Return content of file as string buffer."""
    f = open(path, 'r')
    return f.read()


def find_sdm_refs(buffer):
    return find_refs_reg(buffer,
                         '(Intel SDM Vol. \d[A-Z]{0,1}), (\"[^\"]+\"|\'[^\']+\')',
                         re.MULTILINE)


def find_vtd_refs(buffer):
    return find_refs_reg(buffer,
                         '(Intel VT-d Specification), (\"[^\"]+\"|\'[^\']+\')',
                         re.MULTILINE)


def find_refs_reg(buffer, regex, flags):
    """Find references specified by regex in given string buffer."""

    s = buffer.replace('\n', ' ')
    s = s.replace('--  ', ' ')
    s = s.replace('-- ', ' ')
    s = s.replace('--D ', ' ')
    s = s.replace('// ', ' ')
    s = ' '.join(s.split())
    return re.findall(regex, s, flags)


def check_ref(volume, section):
    """Check given reference by consulting SDM bookmark db."""

    regex = 'BookmarkTitle: ' + section.replace('"', '').replace("'", "")
    regex = '^' + re.escape(regex) + '$'
    cdir = os.path.dirname(os.path.realpath(__file__))

    bookmarks = read_file(cdir + '/bookmarks/' + get_bookmarks(volume))
    return (re.search(regex, bookmarks, re.MULTILINE) != None)
